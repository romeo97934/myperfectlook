const app = angular.module('MainModule', ['ngMaterial', 'ngAnimate', 'ngResource', 'ngRoute']);

app.config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('pink')
        .accentPalette('orange');

    $mdThemingProvider.enableBrowserColor({
        theme: 'default',
        palette: 'accent',
    });
});

app.config(function ($mdIconProvider) {
    $mdIconProvider
        .defaultIconSet('static/svg/mdi.svg')
});

app.config(function ($routeProvider) {
    $routeProvider
        .when('/profile', {
            templateUrl: 'static/template/profile.tmpl.html',
            controller: 'ProfileController'
        })
        .when('/looks', {
            templateUrl: 'static/template/looks.tmpl.html',
            controller: 'LooksController'
        })
        .when('/clothes', {
            templateUrl: 'static/template/clothes.tmpl.html',
            controller: 'ClothesController'
        })
        .when('/feed', {
            templateUrl: 'static/template/feed.tmpl.html',
            controller: 'FeedController'
        })
        .otherwise('/main');
});

app.controller('ConfigController', function ($scope, $rootScope, $location) {
    $rootScope.setLocation = function (location) {
        $location.url(location);
    };

    $scope.config = {'app_name': 'MYPERFECTLOOK'};
});