app.controller('ClothesController', function ($scope) {
    $scope.categoryBottom = {
        'name': 'Bottom wear',
        'icon': 'seat-legroom-reduced'
    };

    $scope.categoryTop = {
        'name': 'Top wear',
        'icon': 'account'
    };

    $scope.clothes = [
        {
            'name': 'Lightweight summer jeans',
            'cover': 'https://cdn-img.instyle.com/sites/default/files/styles/684xflex/public/1495135454/051817-mom-jeans-8.jpg?itok=_HzhDSHP',
            'category': $scope.categoryBottom,
        },
        {
            'name': 'Autumn jacket',
            'cover': 'https://www.patagonia.com/dis/dw/image/v2/ABBM_PRD/on/demandware.static/-/Sites-patagonia-master/default/dw0b78138c/images/hi-res/27805_COI.jpg?sw=750&sh=750&sm=fit&sfrm=png',
            'category': $scope.categoryTop,
        }
    ];
});