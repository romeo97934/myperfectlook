app.controller('FeedController', function ($scope) {
    $scope.feed = [
        {
            'title': 'Item 1 title',
            'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
            'cover': 'https://fashionista.com/.image/t_share/MTQ5MTQxODkyMTcxMzEwNzcx/vo0917_annieleibovitz_logo.jpg',
            'like_counter': 324
        },
        {
            'title': 'Item 2 title',
            'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
            'cover': 'https://resources.stuff.co.nz/content/dam/images/1/l/p/j/s/d/image.related.StuffLandscapeSixteenByNine.620x349.1lpjdn.png/1505707809271.jpg',
            'like_counter': 521
        }
    ];
});